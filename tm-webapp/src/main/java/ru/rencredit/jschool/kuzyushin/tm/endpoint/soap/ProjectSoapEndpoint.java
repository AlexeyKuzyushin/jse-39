package ru.rencredit.jschool.kuzyushin.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @WebMethod
    public Long countAllProjects() {
        return projectService.count();
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects() {
        return projectService.findAll();
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(@WebParam(name = "id", partName = "id") final @Nullable String id) {
        return ProjectDTO.toDTO(projectService.findProjectById(id));
    }

    @WebMethod
    public void createProject(
        @WebParam(name = "projectDTO", partName = "projectDTO") final @Nullable ProjectDTO projectDTO) {
        projectService.create(projectDTO.getUserId(), projectDTO.getName(), projectDTO.getDescription());
    }

    @WebMethod
    public void updateProjectById(
            @WebParam(name = "projectDTO", partName = "projectDTO") final @Nullable ProjectDTO projectDTO) {
        projectService.updateProjectById(projectDTO.getId(), projectDTO.getName(), projectDTO.getDescription());
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "id", partName = "id") final @Nullable String id) {
        projectService.removeProjectById(id);
    }
}
