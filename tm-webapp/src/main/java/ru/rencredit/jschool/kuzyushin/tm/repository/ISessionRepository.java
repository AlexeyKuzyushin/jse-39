package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import javax.transaction.Transactional;
import java.util.List;

public interface ISessionRepository extends JpaRepository<Session, String> {

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

    @NotNull
    List<Session> findAllByUserId(@NotNull String userId);
}
