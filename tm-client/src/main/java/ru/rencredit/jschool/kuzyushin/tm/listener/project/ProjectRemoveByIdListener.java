package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractListener {

    @NotNull
    private final ProjectSoapEndpoint projectSoapEndpoint;

    @Autowired
    public ProjectRemoveByIdListener(
            final @NotNull ProjectSoapEndpoint projectSoapEndpoint
    ) {
        this.projectSoapEndpoint = projectSoapEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        projectSoapEndpoint.removeProjectById(id);
        System.out.println("[OK]");
    }
}
