package ru.rencredit.jschool.kuzyushin.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpointService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpointService;

@Configuration
@ComponentScan(basePackages = "ru.rencredit.jschool.kuzyushin.tm")
public class ClientConfiguration {

    @Bean
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    public TaskSoapEndpoint taskEndpoint(
            @NotNull final TaskSoapEndpointService taskEndpointService
    ) {
        return taskEndpointService().getTaskSoapEndpointPort();
    }

    @Bean
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    public ProjectSoapEndpoint projectEndpoint(
            @NotNull final ProjectSoapEndpointService projectEndpointService
    ) {
        return projectEndpointService().getProjectSoapEndpointPort();
    }
}
